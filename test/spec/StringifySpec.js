describe('SheetClip.stringify', function () {
  describe('test files', function () {

    it('should stringify plain text values (01_simple.json - output from Excel Starter 2010)', function () {
      var test = {
        tsv: 'spec/01_simple.txt',
        json: 'spec/01_simple.json'
      };
      var files = {};

      waitsFor(filesLoaded(test, files));

      runs(function () {
        var parsedJson = JSON.parse(files.json);
        var stringifiedJson = SheetClip.stringify(parsedJson);
        expect(stringifiedJson).toEqual(files.tsv);
      });
    });

    it('should stringify cell with a quoted word (03_quoted_word.json - output from Excel Starter 2010)', function () {
      var test = {
        tsv: 'spec/03_quoted_word.txt',
        json: 'spec/03_quoted_word.json'
      };
      var files = {};

      waitsFor(filesLoaded(test, files));

      runs(function () {
        var parsedJson = JSON.parse(files.json);
        var stringifiedJson = SheetClip.stringify(parsedJson);
        expect(stringifiedJson).toEqual(files.tsv);
      });
    });

    it('should stringify a multiline cell (04_multiline.json - output from Excel Starter 2010)', function () {
      var test = {
        tsv: 'spec/04_multiline.txt',
        json: 'spec/04_multiline.json'
      };
      var files = {};

      waitsFor(filesLoaded(test, files));

      runs(function () {
        var parsedJson = JSON.parse(files.json);
        var stringifiedJson = SheetClip.stringify(parsedJson);
        expect(stringifiedJson).toEqual(files.tsv);
      });
    });

    it('should stringify a multiline cell with a quoted word (05_quoted_multiline.json - output from Excel Starter 2010)', function () {
      var test = {
        tsv: 'spec/05_quoted_multiline.txt',
        json: 'spec/05_quoted_multiline.json'
      };
      var files = {};

      waitsFor(filesLoaded(test, files));

      runs(function () {
        var parsedJson = JSON.parse(files.json);
        var stringifiedJson = SheetClip.stringify(parsedJson);
        expect(stringifiedJson).toEqual(files.tsv);
      });
    });

    it('should stringify a cell that starts with a quote (06_quote_beginning.json - output from Excel Starter 2010)', function () {
      var test = {
        tsv: 'spec/06_quote_beginning.txt',
        json: 'spec/06_quote_beginning.json'
      };
      var files = {};

      waitsFor(filesLoaded(test, files));

      runs(function () {
        var parsedJson = JSON.parse(files.json);
        var stringifiedJson = SheetClip.stringify(parsedJson);
        expect(stringifiedJson).toEqual(files.tsv);
      });
    });

    it('should stringify a cell that ends with a quote (07_quote_ending.json - output from Excel Starter 2010)', function () {
      var test = {
        tsv: 'spec/07_quote_ending.txt',
        json: 'spec/07_quote_ending.json'
      };
      var files = {};

      waitsFor(filesLoaded(test, files));

      runs(function () {
        var parsedJson = JSON.parse(files.json);
        var stringifiedJson = SheetClip.stringify(parsedJson);
        expect(stringifiedJson).toEqual(files.tsv);
      });
    });

    it('should stringify a cell with tab (cell_with_tab.json - output from Excel Starter 2010)', function () {
      var test = {
        tsv: 'spec/cell_with_tab.tsv',
        json: 'spec/cell_with_tab.json'
      };
      var files = {};

      waitsFor(filesLoaded(test, files));

      runs(function () {
        var parsedJson = JSON.parse(files.json);
        var stringifiedJson = SheetClip.stringify(parsedJson);
        expect(stringifiedJson).toEqual(files.tsv);
      });
    });
  });
});