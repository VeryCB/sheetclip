'use strict';

// rewrite of sheetclip.js
(function () {
  "use strict";

  function shouldCloseAppending(s) {
    var res = void 0;
    var re = /"+/g;
    while ((res = re.exec(s)) !== null) {
      if (res[0].length % 2 !== 0) {
        // odd
        return true;
      }
    }
    return false;
  }

  function unescape(s) {
    // unescape "" -> "
    // and remove last single "
    return s.replace(/"{1,2}/g, function (m) {
      return m.length === 2 ? '"' : '';
    });
  }

  function parse(str) {
    var source = str.replace(/\n$/, '');

    var out = [];
    var row = void 0;
    var appending = false;
    var appendingContent = void 0;
    source.split('\n').forEach(function (line) {
      if (!appending) {
        row = [];
        out.push(row);
      }

      line.split('\t').forEach(function (seg, index) {
        if (appending) {
          appendingContent += (index === 0 ? '\n' : '\t') + seg;
          if (shouldCloseAppending(seg)) {
            row.push(unescape(appendingContent));
            appending = false;
            appendingContent = null;
          }
        } else if (/^"/.test(seg)) {
          var toAppend = seg.slice(1);
          if (shouldCloseAppending(toAppend)) {
            row.push(unescape(toAppend));
          } else {
            appending = true;
            appendingContent = toAppend;
          }
        } else {
          row.push(seg);
        }
      });
    });

    if (appending) {
      row.push(unescape(appendingContent));
    }

    return out;
  }

  function stringify(rows) {
    return rows.map(function (row) {
      return row.map(function (s) {
        // quote values with /t /n or leading "
        if (/^"|\t|\n/.test(s)) {
          return '"' + s.replace(/"/g, '""') + '"';
        }
        return s;
      }).join('\t');
    }).join('\n');
  }

  module.exports = {
    parse: parse,
    stringify: stringify
  };
})();

